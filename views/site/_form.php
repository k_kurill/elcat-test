<?php
/**
 * Created by PhpStorm.
 * User: kurill
 * Date: 10.11.17
 * Time: 17:52
 *
 * @var \models\Post $post
 */

?>
<div class="row">
    <form class="form-horizontal" method="post" enctype="multipart/form-data">
        <fieldset>
            <div class="form-group">
                <label for="name" class="col-sm-2 control-label">Title</label>
                <div class="col-sm-8">
                    <input type="text" value="<?=$post->title?>" id="title" class="form-control" name="title" placeholder="Title">
                </div>
            </div>
            <div class="form-group">
                <label for="file" class="col-sm-2 control-label">Image</label>
                <div class="col-sm-8">
                    <input type="file" name="file" id="file">
                </div>
            </div>
            <div class="from-group">
                <label class="col-sm-2 control-label" for="textarea">Text</label>
                <div class="col-sm-8">
                    <textarea class="form-control" id="textarea" rows="3">
                        <?=$post->text?>
                    </textarea>
                </div>
            </div>
        </fieldset>
        <div class="col-sm-offset-2 col-sm-10" style="margin-top: 15px">
            <button type="submit" class="btn btn-primary btn-lg">Submit</button>
        </div>
    </form>
</div>
