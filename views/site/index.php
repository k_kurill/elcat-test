<?php
/**
 * Created by PhpStorm.
 * User: kurill
 * Date: 10.11.17
 * Time: 15:14
 * @var \models\Post[] $posts
 */

?>
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Posts</h1>
        <a class="btn btn-primary" href="/site/create">Add post</a>
    </div>
</div>
<?php foreach ($posts as $post): ?>

<div class="row">
    <div class="col-md-4">
        <a href="#">
            <img class="img-responsive" src="http://placehold.it/700x300" alt="">
        </a>
    </div>
    <div class="col-md-8">
        <h3><?=$post->title?></h3>
        <p><?=$post->text?></p>
        <p><span class="glyphicon glyphicon-time"></span> Posted on <?=$post->updated_at?></p>
        <a class="btn btn-primary" href="/site/view?id=<?=$post->id?>">View Post <span class="glyphicon glyphicon-chevron-right"></span></a>
    </div>
</div>
<hr>
<?php endforeach;?>