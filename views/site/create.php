<?php
/**
 * Created by PhpStorm.
 * User: kurill
 * Date: 10.11.17
 * Time: 17:50
 *
 * @var \models\Post $post
 */
?>
    <h1 class="page-header">
        Create post
    </h1>
<?=\elcat\app\App::$instance->getController()->renderPartialView('_form', ['post' => $post])?>