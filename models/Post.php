<?php
/**
 * Created by PhpStorm.
 * User: kurill
 * Date: 10.11.17
 * Time: 16:25
 */
namespace models;

use elcat\app\DbModel;

class Post extends DbModel
{

    public $text;
    public $updated_at;
    public $title;
    public $image;


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'post';
    }

    /**
     * @return bool
     */
    public function validate()
    {
        // TODO: Implement validate() method.
    }
}