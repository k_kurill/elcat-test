<?php
/**
 * Created by PhpStorm.
 * User: kurill
 * Date: 10.11.17
 * Time: 10:42
 */

require 'SplClassLoader.php';

$packagesLoader = new SplClassLoader();
$packagesLoader->setIncludePath(__DIR__. DIRECTORY_SEPARATOR);
$packagesLoader->register();
