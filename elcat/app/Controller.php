<?php
/**
 * Created by PhpStorm.
 * User: kurill
 * Date: 10.11.17
 * Time: 13:43
 */
namespace elcat\app;

class Controller
{
    public $actionId;
    public $layout = 'main';
    public $defaultAction = 'index';
    public $viewPath;
    public $layoutPath;
    public $id;

    public function __construct($id)
    {
        $this->id = $id;
        $this->viewPath = App::$instance->basePath .
            DIRECTORY_SEPARATOR .
            'views' .
            DIRECTORY_SEPARATOR
            . $this->id;
        $this->layoutPath = App::$instance->basePath .
            DIRECTORY_SEPARATOR .
            'views' .
            DIRECTORY_SEPARATOR
            . 'layouts';
    }

    /**
     * @param $params
     * @throws \Exception
     */
    public function runAction($params)
    {
        if(!$this->actionId) {
            $this->actionId = $this->defaultAction;
        }

        if(!is_callable([$this, $this->actionId])) {
            throw new \Exception("Action not found (id: {$this->actionId})");
        }
        echo call_user_func_array([$this, $this->actionId],$params);
    }

    /**
     * @param $name
     * @param array $params
     * @param bool $return
     * @return string
     */
    public function renderView($name, $params = [], $return = false)
    {
        $content = $this->renderPartialView($name, $params);
        $result = static::renderPhpFile($this->layoutPath . DIRECTORY_SEPARATOR . $this->layout . '.php', ['content' => $content]);
        if ($return) {
            return $result;
        }
        echo $result;
    }

    /**
     * @param $name
     * @param array $params
     * @return string
     */
    public function renderPartialView($name, $params = [])
    {
        $result = static::renderPhpFile($this->viewPath . DIRECTORY_SEPARATOR . $name . '.php', $params);
        return $result;
    }

    /**
     * @param $_file_
     * @param array $_params_
     * @return string
     * @throws \Exception
     * @throws \Throwable
     */
    protected static function renderPhpFile($_file_, $_params_ = [])
    {
        $_obInitialLevel_ = ob_get_level();
        ob_start();
        ob_implicit_flush(false);
        extract($_params_, EXTR_OVERWRITE);
        try {
            require($_file_);
            return ob_get_clean();
        } catch (\Exception $e) {
            while (ob_get_level() > $_obInitialLevel_) {
                if (!@ob_end_clean()) {
                    ob_clean();
                }
            }
            throw $e;
        } catch (\Throwable $e) {
            while (ob_get_level() > $_obInitialLevel_) {
                if (!@ob_end_clean()) {
                    ob_clean();
                }
            }
            throw $e;
        }
    }
}