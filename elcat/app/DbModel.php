<?php
/**
 * Created by PhpStorm.
 * User: kurill
 * Date: 10.11.17
 * Time: 15:43
 */
namespace elcat\app;

abstract class DbModel extends Model
{
    public $primaryKey = 'id';

    /**
     * @return string Name of model's DbTable
     */
    public static abstract function tableName();

    /**
     * @return \PDO;
     */
    public static function getDb()
    {
        return App::$instance->getDb();
    }

    public function insert()
    {

    }

    public function update()
    {

    }

    public static function delete()
    {

    }

    public static function findOne($attributes=[], $select='*')
    {
        $result = static::find($attributes, $select, 1);
        if(is_array($result) && $result) {
            return $result[0];
        }
        return null;
    }

    public static function find($attributes=[], $select='*', int $limit = null, int $offset = null)
    {
        $db = static::getDb();
        $tableName = static::tableName();

        if(is_array($select)) {
            $select = implode(', ',$select);
        }
        $queryStr = "SELECT $select FROM $tableName WHERE ";

        $whereFields = array_keys($attributes);
        $where = [];
        foreach ($whereFields as $field) {
            $where[]= $field . ' = :' . $field;
        }
        $queryStr .= ($where)?implode(' AND ', $where):'TRUE';

        if($limit) {
            $queryStr .= ' LIMIT :limit';
        }
        if($offset) {
            $queryStr .= ' OFFSET :offset';
        }

        $statement = $db->prepare($queryStr);
        foreach ($attributes as $field => $value) {
            $statement->bindValue(':'.$field,$value);
        }

        if($limit) {
            $statement->bindValue(':limit',$limit, \PDO::PARAM_INT);
        }
        if($offset) {
            $statement->bindValue(':offset',$offset, \PDO::PARAM_INT);
        }
        $statement->execute();
        return $statement->fetchAll(\PDO::FETCH_CLASS, static::class);
    }
}