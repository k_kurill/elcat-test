<?php
/**
 * Created by PhpStorm.
 * User: kurill
 * Date: 10.11.17
 * Time: 12:05
 * Class App
 * @package elcat\app
 */
namespace elcat\app;

use elcat\db\Db;
use elcat\request\Request;

class App
{
    public $defaultRoute = 'site/index';
    public $controllerNamespace = 'controllers';
    public $basePath;

    public static $instance;

    private $_db = null;
    private $_config;
    private $_controller;

    public function __construct($config)
    {
        $this->_config = $config;
        $this->basePath = realpath(__DIR__ . '/../..');
        self::$instance = $this;
    }

    /**
     * Run the application
     */
    public function run()
    {
        if(isset($this->_config['db'])) {
            $this->_db = $this->initDb();
            $this->_db->openConnection();
        }
        $this->handleRequest(new Request());
    }

    /**
     * @param Request $request
     */
    private function handleRequest(Request $request)
    {
        $this->runAction($request->route, $request->queryParams);
    }

    /**
     * Initialize connection to database
     * @return Db
     */
    private function initDb()
    {
        $dbConf = $this->_config['db'];
        return new Db(
            $dbConf['dsn'],
            $dbConf['username'],
            $dbConf['password'],
            isset($dbConf['charset'])?$dbConf['charset']:null
        );
    }

    /**
     * @param string $route
     * @param array $params
     * @return mixed
     * @throws \Exception
     */
    public function runAction($route, $params = [])
    {
        $this->_controller = $this->createController($route);

        if ($this->_controller instanceof Controller) {

            $result = $this->_controller->runAction($params);
            return $result;
        }

        throw new \Exception('Unable to resolve the request "' . ($route) . '".');
    }

    /**
     * @return \PDO
     */
    public function getDb()
    {
        return $this->_db->getDb();
    }

    /**
     * @param string $route
     * @return Controller
     */
    private function createController($route)
    {
        if ($route === '') {
            $route = $this->defaultRoute;
        }

        if (strpos($route, '/') !== false) {
            list($id, $route) = explode('/', $route, 2);
        } else {
            $id = $route;
            $route = '';
        }

        $className = ucfirst($id);
        $className = ltrim($this->controllerNamespace . '\\' . $className, '\\');

        $controller = new $className($id);
        $controller->actionId = $route;
        return $controller;
    }

    /**
     * @return Controller
     */
    public function getController()
    {
        return $this->_controller;
    }
}

