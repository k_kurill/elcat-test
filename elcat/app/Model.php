<?php
/**
 * Created by PhpStorm.
 * User: kurill
 * Date: 10.11.17
 * Time: 15:45
 */
namespace elcat\app;

abstract class Model
{
    /**
     * @return bool
     */
    public abstract function validate();
}