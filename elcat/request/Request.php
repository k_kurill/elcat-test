<?php
/**
 * Created by PhpStorm.
 * User: kurill
 * Date: 10.11.17
 * Time: 12:01
 */
namespace elcat\request;

class Request
{

    public $requestUri;
    public $route;
    public $queryParams;

    public function __construct()
    {
        $this->requestUri = $_SERVER['REQUEST_URI'];
        $this->parseRequest();
    }

    private function parseRequest()
    {
        $path = parse_url($this->requestUri, PHP_URL_PATH);
        $this->route = trim($path, '/');
        parse_str($_SERVER['QUERY_STRING'], $this->queryParams);
    }
}