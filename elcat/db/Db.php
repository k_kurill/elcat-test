<?php
/**
 * Created by PhpStorm.
 * User: kurill
 * Date: 10.11.17
 * Time: 10:46
 */
namespace elcat\db;

class Db
{
    private $_pdo;
    private $_dsn;
    private $_username;
    private $_password;
    private $_charset;

    public function __construct($dsn, $username, $password, $charset = null)
    {
        $this->_dsn = $dsn;
        $this->_username = $username;
        $this->_password = $password;
        $this->_charset = $charset;

    }

    public function getDb()
    {
        return $this->_pdo;
    }

    public function openConnection()
    {
        $this->_pdo = $this->createPdoInstance();
        $this->initConnection();
    }

    /**
     * Creates the PDO instance.
     * @return PDO the pdo instance
     */
    protected function createPdoInstance()
    {
        return new \PDO($this->_dsn, $this->_username, $this->_password);
    }

    /**
     * Initializes the DB connection.
     * This method is invoked right after the DB connection is established.
     */
    protected function initConnection()
    {
        if ($this->_charset !== null) {
            $this->_pdo->exec('SET NAMES ' . $this->_charset);
        }
    }

}