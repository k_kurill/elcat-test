<?php
/**
 * Created by PhpStorm.
 * User: kurill
 * Date: 10.11.17
 * Time: 10:43
 */
require 'autoload.php';

use elcat\app\App;


$config = require (__DIR__."/config/main.php");
(new App($config))->run();