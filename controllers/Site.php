<?php
namespace controllers;
use elcat\app\Controller;
use models\Post;

/**
 * Created by PhpStorm.
 * User: kurill
 * Date: 10.11.17
 * Time: 13:18
 */

class Site extends Controller
{
    public function index()
    {
        $posts = Post::find();
        return $this->renderView('index',  [ 'posts' =>$posts]);
    }

    public function view($id)
    {
        $post = Post::findOne(['id'=>$id]);
        return $this->renderView('view',  [ 'post' =>$post]);
    }

    public function create()
    {
        $post = new Post();
        return $this->renderView('create',  [ 'post' =>$post]);
    }
}